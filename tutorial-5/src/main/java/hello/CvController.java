package hello;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    Map<String, String> cvInfos = new HashMap<>();

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        if (visitor == null || visitor.equals("")) {
            model.addAttribute("visitorMessage", "This is my CV");
        } else {
            model.addAttribute("visitorMessage", visitor + ", I hope youre interested to hire me");
        }
        buildAttrs();
        model.addAllAttributes(cvInfos);
        return "cv";

    }

    private void buildAttrs() {
        cvInfos.put("owner", "Samuel Tupa Febrian");
        cvInfos.put("birthDate", "7 February 1998");
        cvInfos.put("birthPlace", "Jakarta");
        cvInfos.put("address", "Jl. Pemuda M2");
        cvInfos.put("elemSchool", "SD Tarakanita 5");
        cvInfos.put("midSchool", "SMP Tarakanita 4");
        cvInfos.put("highSchool", "Canisius College");
        cvInfos.put("univ", "Universitas Indonesia");
        String essay = "So... as you can see, I am 20 Years old."
                + "I really like watching Association Football, even until now."
                + "I also like Anime and Manga, though recently I'm interested in Football more."
                + "I am currently enroll Computer Science Faculty of Universitas Indonesia, "
                + "and at Fourth Semester. To be honest I'm very interested on Internship program,"
                + " and hope that I could actually do that on this summer.";
        cvInfos.put("essay", essay);
    }

}
