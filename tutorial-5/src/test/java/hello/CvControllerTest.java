
package hello;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvWithVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "Tamamo"))
                .andExpect(content().string(containsString("Tamamo, I hope youre interested to hire me")))
                .andExpect(content().string(not("This is my CV")));
    }

    @Test
    public void cvWithoutVisitor() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(", I hope youre interested to hire me")));
    }

    @Test
    public void cvWithNoNameVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", ""))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(", I hope youre interested to hire me")));
    }

    @Test
    public void checkCVFullName() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Samuel Tupa Febrian")))
                .andExpect(content().string(not("Okita Souji")));
    }

    @Test
    public void checkCVBirthDate() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("7 February 1998")))
                .andExpect(content().string(not("32 Desember 3092")));
    }

    @Test
    public void checkCVBirthPlace() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Jakarta")))
                .andExpect(content().string(not("Narita")));
    }

    @Test
    public void checkCVAddress() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Jl. Pemuda M2")))
                .andExpect(content().string(not("Sakurasou Street")));
    }

    @Test
    public void checkCVElementarySchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("SD Tarakanita 5")))
                .andExpect(content().string(not("Javari Park")));
    }

    @Test
    public void checkCVMiddleSchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("SMP Tarakanita 4")))
                .andExpect(content().string(not("Teiko Junior High School")));
    }

    @Test
    public void checkCVHighSchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Canisius College")))
                .andExpect(content().string(not("Sanada North High School")));
    }

    @Test
    public void checkCVUniversity() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Universitas Indonesia")))
                .andExpect(content().string(not("BSI")));
    }

    @Test
    public void checkCVEssay() throws Exception {
        String goodEssay = "So... as you can see, I am 20 Years old."
                + "I really like watching Association Football, even until now." +
                "I also like Anime and Manga, though recently I'm interested in Football more." +
                "I am currently enroll Computer Science Faculty of Universitas Indonesia, " +
                "and at Fourth Semester. To be honest I'm very interested on Internship program," +
                " and hope that I could actually do that on this summer.";
        String badEssay = "Hasire sori yo~\n"+"Kaze no you ni~\n"+"Tsuki mihara wo~\n"+"PADORU PADORU!!!!";

        mockMvc.perform(get("/cv"))
                .andExpect(content().string(not(goodEssay)))
                .andExpect(content().string(not(badEssay)));
    }

}