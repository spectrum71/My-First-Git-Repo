class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double calculateAmount() {
        // Determine amount for each line
        double amount = 0;
        switch (getMovie().getPriceCode()) {
            case Movie.REGULAR:
                amount += 2;
                if (getDaysRented() > 2) {
                    amount += (getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                amount += getDaysRented() * 3;
                break;
            case Movie.CHILDREN:
                amount += 1.5;
                if (getDaysRented() > 3) {
                    amount += (getDaysRented() - 3) * 1.5;
                }
                break;
            default:
                break;
        }
        return amount;
    }

    public int getRenterPoints() {
        return 1 + addBonus();
    }

    private int addBonus() {
        // Add bonus for a two day new release rental
        if ((getMovie().getPriceCode() == Movie.NEW_RELEASE)
                && getDaysRented() > 1) {
            return 1;
        }
        return 0;
    }

    public String chargeBon(int type) {
        return getIndent(type) + getMovie().getTitle() + getDataSeparator(type)
                + String.valueOf(calculateAmount());
    }

    private String getIndent(int type) {
        String output = "";
        switch (type) {
            case Customer.HTML:
                output = "     ";
                break;
            case Customer.STANDARD:
                output = "\t";
                break;
            default:
                break;
        }
        return output;
    }

    private String getDataSeparator(int type) {
        String output = "";
        switch (type) {
            case Customer.HTML:
                output = " : ";
                break;
            case Customer.STANDARD:
                output = "\t";
                break;
            default:
                break;
        }
        return output;
    }
}