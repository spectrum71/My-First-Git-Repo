import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Customer {

    public static final int STANDARD = 0;
    public static final int HTML = 1;
    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        // Start with header lines
        String result = getHeader(STANDARD);

        // Add all the charge bons
        result += getAllChargeBon(STANDARD);

        // Add footer lines
        result += getFooter(STANDARD);

        return result;
    }

    public String htmlStatement() {
        // Start with header lines
        String result = "<h1>" + getHeader(HTML) + "</h1>";

        // Add all the charge bons
        result += "<p>" + getAllChargeBon(HTML) + "</p>";

        // Add footer lines
        result += "<h2>" + getFooter(HTML) + "</h2>";

        return result;
    }

    private double getTotalAmount() {
        return rentals.stream().mapToDouble(each -> each.calculateAmount()).sum();
    }

    private int getFrequentRenterPoints() {
        return rentals.stream().mapToInt(each -> each.getRenterPoints()).sum();
    }

    private String getAllChargeBon(int type) {
        return rentals.stream()
                .map(each -> each.chargeBon(type))
                .collect(Collectors.joining(getSeparator(type)))
                + getSeparator(type);
    }

    private String getHeader(int type) {
        return "Rental Record for " + getName() + getSeparator(type);
    }

    private String getFooter(int type) {
        return "Amount owed is "
                + String.valueOf(getTotalAmount())
                + getSeparator(type)
                + "You earned "
                + String.valueOf(getFrequentRenterPoints())
                + " frequent renter points";
    }

    private String getSeparator(int type) {
        String output = "";
        switch (type) {
            case HTML:
                output = "<br>";
                break;
            case STANDARD:
                output = "\n";
                break;
            default:
                break;
        }
        return output;
    }

}