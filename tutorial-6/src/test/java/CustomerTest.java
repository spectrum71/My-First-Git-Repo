import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    Customer customer;
    Movie movie;
    Movie movie2;
    Movie movie3;
    Rental rent;
    Rental rent2;
    Rental rent3;
    String result;
    String[] lines;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        movie2 = new Movie("Sopo Jenengmu?", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 7);
        movie3 = new Movie("Anak Belajar Shogi", Movie.CHILDREN);
        rent3 = new Rental(movie3, 5);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        result = customer.statement();
        lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!

    @Test
    public void statementWithMultipleMovies() {
        // TODO Implement me!

        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);

        result = customer.statement();
        lines = result.split("\n");
        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 29"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        result = customer.htmlStatement();
        lines = result.split("<br>");
        System.out.println(result);
        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        // TODO Implement me!

        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        System.out.println(result);
        result = customer.htmlStatement();
        lines = result.split("<br>");
        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 29"));
        assertTrue(result.contains("4 frequent renter points"));
    }
}