package tutorial.javari;

public class JsonMessage {
    private final String msgType;
    private final String message;

    private JsonMessage(String msgType, String message) {
        this.msgType = msgType;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getMsgType() {
        return msgType;
    }

    public static JsonMessage getNotFoundMessage(int id) {
        return new JsonMessage("not found warning",
                "Sorry, but it seems we have no animal with id no." + id);
    }

    public static JsonMessage getDatabaseEmptyMessage() {
        return new JsonMessage("empty DB warning",
                "Sorry, but it seems there's no animal in our database...");
    }

    public static JsonMessage getDuplicateIdMessage() {
        return new JsonMessage("duplicate warning",
                "Sorry, but it seems there's an animal with such id already...");
    }

    public static JsonMessage getSuccessDeleteMessage() {
        return new JsonMessage("deletion success",
                "We have successfully deleted the animal from our database. "
                        + "Here's the data in case you want to create it again some time");
    }

    public static JsonMessage getSuccessAddMessage() {
        return new JsonMessage("insertion success",
                "We have successfully added the animal into our database. "
                        + "Here's the registered data of the animal");
    }

}
