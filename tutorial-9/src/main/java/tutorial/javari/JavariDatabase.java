package tutorial.javari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class JavariDatabase {

    private List<Animal> animals;
    private final String dataPath = "javari_data.csv";
    private final Path file = Paths.get("", dataPath);

    public JavariDatabase() throws IOException {
        animals = new ArrayList<>();
        loadData();
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public Animal getAnimalById(int id) {
        return animals.stream()
                .filter(a -> a.getId() == id)
                .findAny().orElse(null);
    }

    public Animal addAnimal(String json) throws IOException {
        Animal newAnimal = jsonToAnimal(json);
        if (!duplicateId(newAnimal)) {
            animals.add(newAnimal);
            saveData();
            return newAnimal;
        }
        return null;
    }

    public Animal deleteAnimalById(int id) throws IOException {
        Animal animal = animals.stream().filter(a -> a.getId() == id)
                .findAny().orElse(null);
        if (animal != null) {
            animals.removeIf(a -> a.getId() == id);
            saveData();
        }
        return animal;
    }

    //===================== Private Line ===========================

    private void loadData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file.toString()));
        String line = reader.readLine();

        while (line != null) {
            animals.add(csvToAnimal(line));
            line = reader.readLine();
        }

        reader.close();
    }

    private void saveData() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file.toString()));
        for (Animal animal : animals) {

            String csv = animalToCsv(animal);

            writer.write(csv);
            writer.newLine();
        }
        writer.close();
    }

    private boolean duplicateId(Animal subject) {
        return animals.stream()
                .anyMatch(a -> a.getId() == subject.getId());
    }

    private Animal jsonToAnimal(String input) {
        JSONObject json = new JSONObject(input);
        return new Animal(json.getInt("id"), json.getString("type"),
                json.getString("name"),
                Gender.parseGender(json.getString("gender")),
                json.getDouble("length"), json.getDouble("weight"),
                Condition.parseCondition(json.getString("condition")));
    }

    private Animal csvToAnimal(String csvInput) {
        String[] attrs = csvInput.split(",");
        return new Animal(Integer.parseInt(attrs[0]),
                attrs[1], attrs[2], Gender.parseGender(attrs[3]),
                Double.parseDouble(attrs[4]), Double.parseDouble(attrs[5]),
                Condition.parseCondition(attrs[6]));
    }

    private String animalToCsv(Animal animal) {
        String[] attrs = {animal.getId().toString(), animal.getType(),
                animal.getName(), animal.getGender().toString(),
                String.valueOf(animal.getLength()),
                String.valueOf(animal.getWeight()),
                animal.getCondition().toString()};
        return String.join(",", attrs);
    }

}
