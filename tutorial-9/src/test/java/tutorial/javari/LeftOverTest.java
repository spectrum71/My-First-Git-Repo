package tutorial.javari;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import tutorial.Application;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class LeftOverTest {

    private String[] environment = {
        "--spring.main.web-environment=false", "--spring.autoconfigure.exclude=blahblahblah"
    };

    private boolean mainWrapper(String[] args) {
        Application.main(args);
        return true;
    }

    //Application test
    @Test
    public void testMainMethodIsWorking() {
        assertTrue(mainWrapper(environment));

    }

    @Test
    public void testMainMethodIsNotWorking() {
        boolean isCatched = false;
        try {
            isCatched = !mainWrapper(null);
        } catch (IllegalArgumentException e) {
            isCatched = true;
        }
        assertTrue(isCatched);
    }

    //Enumerator test
    @Test
    public void testUnsupportedStringCondition() {
        boolean isCatched = false;
        try {
            Condition.parseCondition("random");
        } catch (UnsupportedOperationException e) {
            isCatched = true;
        }
        assertTrue(isCatched);
    }

    @Test
    public void testUnsupportedStringGender() {
        boolean isCatched = false;
        try {
            Gender.parseGender("trap");
        } catch (UnsupportedOperationException e) {
            isCatched = true;
        }
        assertTrue(isCatched);
    }

}
