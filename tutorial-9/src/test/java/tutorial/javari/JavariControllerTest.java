package tutorial.javari;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    // TODO Implement me! (additional task)

    @Autowired
    private MockMvc mockMvc;

    private JavariDatabase database = new JavariDatabase();
    private List<String> animalsJson = new ArrayList<>();

    @Before
    public void setUp() {
        ObjectMapper mapper = new ObjectMapper();
        database.getAnimals()
                .forEach(a -> {
                    try {
                        animalsJson.add(mapper.writeValueAsString(a));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });
    }

    public JavariControllerTest() throws IOException {
    }

    @Test
    public void getAnimalByIdTest() throws Exception {
        this.mockMvc.perform(get("/javari/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("2"));
    }

    @Test
    public void getByIdNotFoundTest() throws Exception {
        this.mockMvc.perform(get("/javari/203"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.msgType")
                        .value("not found warning"));
    }

    @Test
    public void getAllAnimalsTest() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].id").value("2"));
    }

    @Test
    public void deleteByIdNotFoundTest() throws Exception {
        this.mockMvc.perform(delete("/javari/203"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.msgType")
                        .value("not found warning"));
    }

    /*
    Because the test affects the database, we need to do delete and add sequentially
    In this case, delete the existing data and then add it back
     */
    @Test
    public void deleteAddEmptyTests() throws Exception {
        deleteByIdTest();
        emptyDataTest();
        addAnimalTest();
        addDuplicateTest();
    }

    public void deleteByIdTest() throws Exception {
        for (Animal animal : database.getAnimals()) {
            int id = animal.getId();
            this.mockMvc.perform(delete("/javari/" + id))
                    .andDo(print()).andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].msgType")
                            .value("deletion success"))
                    .andExpect(jsonPath("$[1].id").value(id));

            this.mockMvc.perform(get("/javari/" + id))
                    .andDo(print()).andExpect(status().isOk())
                    .andExpect(jsonPath("$.msgType")
                            .value("not found warning"));
        }

    }

    public void emptyDataTest() throws Exception {
        this.mockMvc.perform(get("/javari"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.msgType")
                        .value("empty DB warning"));
    }

    public void addAnimalTest() throws Exception {
        int index = 0;
        for (Animal animal : database.getAnimals()) {
            int id = animal.getId();
            this.mockMvc.perform(post("/javari").content(animalsJson.get(index)))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].msgType")
                            .value("insertion success"))
                    .andExpect(jsonPath("$[1].id").value(id));

            this.mockMvc.perform(get("/javari/" + id))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(id));
            index++;
        }

    }

    public void addDuplicateTest() throws Exception {
        this.mockMvc.perform(post("/javari").content(animalsJson.get(0)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.msgType")
                        .value("duplicate warning"));
    }
}