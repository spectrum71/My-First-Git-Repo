import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 2nd exercise.
 */
public class WordCount {

    public static long countLines(String word, Path file) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(file.toString()));

        long count = reader.lines().filter(i -> i.contains(word)).count();
        reader.close();
        return count;
    }

    public static void main(String[] args) throws IOException {
        String word = "for";
        String filePath = "aTextFile.txt";
        Path file = Paths.get("", filePath);

        long count = countLines(word, file);

        System.out.println(String.format("The word substring '%s' occurred in %d lines",
                word, count));
    }
}
