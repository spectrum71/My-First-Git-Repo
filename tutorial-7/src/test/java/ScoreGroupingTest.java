import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!

    Map<String, Integer> scores = new HashMap<>();
    Map<Integer, List<String>> group = new HashMap<>();

    public boolean mainChecker() {
        ScoreGrouping.main(null);
        return true;
    }

    @Before
    public void setUp() {
        scores.put("Tama", 10);
        scores.put("Bryn", 15);
        scores.put("Jannu", 17);
        scores.put("Okita", 15);
        scores.put("Nobbu", 12);
        scores.put("Roma", 12);
        scores.put("Homie", 10);
        scores.put("Gil", 10);

        group = ScoreGrouping.groupByScores(scores);

    }

    @Test
    public void testIfNamesGroupedProperly() {
        assertTrue(group.get(10).contains("Tama"));
        assertTrue(group.get(10).contains("Homie"));
        assertTrue(group.get(10).contains("Gil"));

        assertTrue(group.get(12).contains("Nobbu"));
        assertTrue(group.get(12).contains("Roma"));

        assertTrue(group.get(15).contains("Bryn"));
        assertTrue(group.get(15).contains("Okita"));

        assertTrue(group.get(17).contains("Jannu"));

    }

    @Test
    public void checkMainMethodIsWorking() {
        assertTrue(mainChecker());
    }
}