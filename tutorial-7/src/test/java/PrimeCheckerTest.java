import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

public class PrimeCheckerTest {

    private static final List<Integer> PRIME_NUMBERS = Arrays.asList(2, 3, 5, 7);
    private static final List<Integer> COMPOSITE_NUMBERS = Arrays.asList(4, 6, 8, 9, 10);

    public boolean mainChecker() {
        PrimeChecker.main(null);
        return true;
    }

    @Test
    public void testIsPrimeTrueGivenPrimeNumbers() {
        PRIME_NUMBERS.forEach(number ->
                assertTrue(PrimeChecker.isPrime(number)));
    }

    @Test
    public void testIsPrimeFalseGivenNonPrimeNumbers() {
        COMPOSITE_NUMBERS.forEach(number ->
                assertFalse(PrimeChecker.isPrime(number)));
        // Given non-prime numbers
        // When isPrime is invoked
        // It should return false
    }

    @Test
    public void checkMainMethodIsRunning() {
        TestCase.assertTrue(mainChecker());
    }
}