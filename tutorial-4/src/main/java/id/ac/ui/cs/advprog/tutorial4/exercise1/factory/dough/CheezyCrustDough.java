package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CheezyCrustDough implements Dough {
    public String toString() {
        return "Cheezy Crust Dough for moar cheese";
    }
}
