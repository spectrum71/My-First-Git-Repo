package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SteamedClam implements Clams {
    public String toString() {
        return "Steamed Clams because why not?";
    }
}
