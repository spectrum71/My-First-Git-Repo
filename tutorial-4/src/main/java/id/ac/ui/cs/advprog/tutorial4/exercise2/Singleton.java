package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?

    //Lazy version
    //private static Singleton uniqueInstance;

    //Eager version
    private static Singleton uniqueInstance = new Singleton();

    private Singleton() {
    }

    public static Singleton getInstance() {
        // TODO Implement me!
        //Lazy version's double checking
        // if (uniqueInstance == null) {
        //     uniqueInstance = new Singleton();
        // }

        return uniqueInstance;
    }
}
