package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class SzechuanSauce implements Sauce {
    public String toString() {
        return "THE GLORIOUS SZECHUAN SAUCE HAS RETURNED";
    }
}
