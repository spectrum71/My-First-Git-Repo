package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NewYorkPizzaStoreTest {
    PizzaStore nyStore;
    Pizza clamPizza;
    Pizza cheesePizza;
    Pizza veggiePizza;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
        clamPizza = nyStore.orderPizza("clam");
        cheesePizza = nyStore.orderPizza("cheese");
        veggiePizza = nyStore.orderPizza("veggie");
    }

    @Test
    public void checkPizzaInstances() {
        assertTrue(cheesePizza instanceof CheesePizza);
        assertTrue(clamPizza instanceof ClamPizza);
        assertTrue(veggiePizza instanceof VeggiePizza);
    }

    @Test
    public void checkPizzaNames() {
        assertEquals("New York Style Cheese Pizza", cheesePizza.getName());
        assertEquals("New York Style Veggie Pizza", veggiePizza.getName());
        assertEquals("New York Style Clam Pizza", clamPizza.getName());
    }

    @Test
    public void checkPizzaDescriptions() {
        assertNotNull(clamPizza.toString());
        assertNotNull(veggiePizza.toString());
        assertNotNull(cheesePizza.toString());
    }
}