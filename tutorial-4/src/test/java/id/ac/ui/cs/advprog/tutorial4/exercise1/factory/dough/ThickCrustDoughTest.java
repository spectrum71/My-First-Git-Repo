package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThickCrustDoughTest {

    Dough thickCrustDough;

    @Before
    public void setUp() {
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("ThickCrust style extra thick crust dough", thickCrustDough.toString());
    }
}