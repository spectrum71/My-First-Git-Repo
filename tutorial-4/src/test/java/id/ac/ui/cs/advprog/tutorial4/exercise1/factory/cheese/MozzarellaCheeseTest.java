package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MozzarellaCheeseTest {

    Cheese mozarella;

    @Before
    public void setUp() {
        mozarella = new MozzarellaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Mozzarella",mozarella.toString());
    }
}