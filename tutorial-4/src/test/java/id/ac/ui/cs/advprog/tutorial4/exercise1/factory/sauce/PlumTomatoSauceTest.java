package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlumTomatoSauceTest {

    Sauce plumTomatoSauce;

    @Before
    public void setUp() {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Tomato sauce with plum tomatoes", plumTomatoSauce.toString());
    }
}