package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FrozenClamsTest {

    Clams frozenClams;

    @Before
    public void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    public void testToString() {
        assertEquals("Frozen Clams from Chesapeake Bay",frozenClams.toString());
    }
}