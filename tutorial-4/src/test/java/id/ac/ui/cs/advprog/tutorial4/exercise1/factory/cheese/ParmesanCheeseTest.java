package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParmesanCheeseTest {

    Cheese parmesan;

    @Before
    public void setUp() {
        parmesan = new ParmesanCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Parmesan",parmesan.toString());
    }
}