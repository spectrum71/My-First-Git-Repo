package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PizzaTestDriveTest {
    PizzaTestDrive testDrive;
    @Before
    public void setUp() {
        testDrive = new PizzaTestDrive();
    }
    @Test
    public void testMain() {
       assertTrue(testDrive.mainIsWorking());
    }
}