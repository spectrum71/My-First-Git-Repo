package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DepokPizzaStoreTest {
    PizzaStore depokStore;
    Pizza clamPizza;
    Pizza cheesePizza;
    Pizza veggiePizza;

    @Before
    public void setUp() {
        depokStore = new DepokPizzaStore();
        clamPizza = depokStore.orderPizza("clam");
        cheesePizza = depokStore.orderPizza("cheese");
        veggiePizza = depokStore.orderPizza("veggie");

    }

    @Test
    public void checkPizzaInstances() {
        assertTrue(cheesePizza instanceof CheesePizza);
        assertTrue(clamPizza instanceof ClamPizza);
        assertTrue(veggiePizza instanceof VeggiePizza);
    }

    @Test
    public void checkPizzaNames() {

        assertEquals("Depok Style Cheese Pizza", cheesePizza.getName());
        assertEquals("Depok Style Veggie Pizza", veggiePizza.getName());
        assertEquals("Depok Style Clam Pizza", clamPizza.getName());
    }

    @Test
    public void checkPizzaDescriptions() {
        assertNotNull(clamPizza.toString());
        assertNotNull(veggiePizza.toString());
        assertNotNull(cheesePizza.toString());
    }
}