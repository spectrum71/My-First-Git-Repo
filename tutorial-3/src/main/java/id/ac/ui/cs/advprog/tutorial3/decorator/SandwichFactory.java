package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class SandwichFactory {
    public static void main(String[] args) {
        Food bun = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(bun.getDescription());
        System.out.println("Total Cost : " + bun.cost());

        Food meatBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(bun);
        System.out.println(meatBurger.getDescription());
        System.out.println("Total Cost : " + meatBurger.cost());

        Food cheeseBurger = FillingDecorator.CHEESE.addFillingToBread(meatBurger);
        System.out.println(cheeseBurger.getDescription());
        System.out.println("Total Cost : " + cheeseBurger.cost());

        Food meatMaxBurger = FillingDecorator.CHICKEN_MEAT.addFillingToBread(meatBurger);
        System.out.println(meatMaxBurger.getDescription());
        System.out.println("Total Cost : " + meatMaxBurger.cost());
    }
}
