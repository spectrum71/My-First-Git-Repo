package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    private double salary;
    private static final double MINIMUM_SALARY = 200000.00;

    public Ceo(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "CEO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
