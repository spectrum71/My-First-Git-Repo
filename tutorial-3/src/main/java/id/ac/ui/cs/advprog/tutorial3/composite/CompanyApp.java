package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;

public class CompanyApp {
    public static void main(String[] args) {
        Company startUp = new Company();

        Employees okita = new Ceo("Okita Souji", 1000000.00);
        startUp.addEmployee(okita);
        System.out.println(okita.getSalary());

        Employees tamamo = new Cto("Tamamo no Mae", 500000);
        startUp.addEmployee(tamamo);
        System.out.println(tamamo.getName());

        Employees liz = new BackendProgrammer("Elizabeth Bathory", 300000);
        System.out.println(liz.getRole());

        Employees jeanne = new NetworkExpert("Jeanne D'Arc", 100000);
        startUp.addEmployee(jeanne);

        try {
            Employees jalter = new SecurityExpert("Jeanne D'Arc (Alter)", 1);
            startUp.addEmployee(jalter);
        } catch (IllegalArgumentException e) {
            //do nothing
        }

        System.out.println(startUp.getNetSalaries());
    }
}
