package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    private double salary;
    private static final double MINIMUM_SALARY = 70000.00;

    public SecurityExpert(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
