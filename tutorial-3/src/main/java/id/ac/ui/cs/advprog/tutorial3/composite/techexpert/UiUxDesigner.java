package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    private double salary;
    private static final double MINIMUM_SALARY = 90000.00;

    public UiUxDesigner(String name, double salary) throws IllegalArgumentException {
        //TODO Implement
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException();
        }
        this.name = name;
        this.salary = salary;
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}